//
//  AppDelegate.m
//  Currency
//
//  Created by Tianyu An on 16/1/29.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import "AppDelegate.h"
#import "MainTableViewController.h"
#import "ExchangeViewController.h"
#import "DBManager.h"
#import "CurrencyDefaults.h"
#import "Currency.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    if (@available(iOS 15.0, *)) {
        UINavigationBarAppearance *appearance = [[UINavigationBarAppearance alloc] init];
        [appearance configureWithOpaqueBackground];
        appearance.backgroundColor = [UIColor whiteColor];
        appearance.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:MAJOR_COLOR,
                                          NSForegroundColorAttributeName, nil];
        [[UINavigationBar appearance] setStandardAppearance:appearance];
        [[UINavigationBar appearance] setScrollEdgeAppearance:appearance];
    }
    if (@available(iOS 13.0, *)) {
        self.window.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
        
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setTintColor:MAJOR_COLOR];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:MAJOR_COLOR, NSForegroundColorAttributeName, nil]];
    
    [[UITableView appearance] setSectionIndexColor:MAJOR_COLOR];
    [[UITableView appearance] setSectionIndexBackgroundColor:[UIColor clearColor]];
    [[UITableView appearance] setSeparatorInset:UIEdgeInsetsZero];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"dataMigrated"]) {
        [self migrateData];
    }
    
    [[CurrencyDefaults getInstance] initData];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(nonnull NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    NSString *prefix = @"currency://currency=";
    if ([[url absoluteString] rangeOfString:prefix].location != NSNotFound) {
        NSString *code = [[url absoluteString] substringFromIndex:prefix.length];
        Currency *currency = [[DBManager getInstance] getCurrencyByCode:code];
    
        ExchangeViewController *controller = [[ExchangeViewController alloc] init];
        controller.currency = currency;
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
        self.window.rootViewController.navigationItem.backBarButtonItem = backButton;
        
        UINavigationController *navigationController = (UINavigationController *) self.window.rootViewController;
        [navigationController popToRootViewControllerAnimated:NO];
        [navigationController pushViewController:controller animated:YES];
    }
    return YES;
}

- (void)migrateData {
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    CurrencyDefaults *shareDefaults = [CurrencyDefaults getInstance];
    
    int type = [[NSUserDefaults standardUserDefaults] boolForKey:@"middleRateSwitch"] ? TYPE_CASH_SELLING : TYPE_MIDDLE;
    [shareDefaults setEmphasizeType:type];
    [shareDefaults setLastUpdateTime:[standardUserDefaults integerForKey:@"lastUpdateTime"]];
    [shareDefaults synchronize];
    
    [DBManager migrateData];
    
    [standardUserDefaults setBool:YES forKey:@"dataMigrated"];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
