//
//  AppDelegate.h
//  Currency
//
//  Created by Tianyu An on 16/1/29.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewUtils.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
