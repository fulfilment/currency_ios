//
//  DBManager.h
//  Rate_ios
//
//  Created by lichangyuan on 16/1/28.
//  Copyright © 2016年 lijingjie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDB.h"
#import "Currency.h"

typedef void (^SuccessBlock)(id respondObject);
typedef void (^FailureBlock)(NSError *error);

@interface DBManager : NSObject

+ (DBManager *)getInstance;
+ (void)migrateData;

- (void)updateCurrency:(Currency *)currency;
- (void)updateCurrencyShowInfo:(Currency *)currency;
- (Currency *)getCurrencyByName:(NSString *)name;
- (Currency *)getCurrencyByCode:(NSString *)code;
- (NSMutableArray *)getShownCurrencyList;
- (NSMutableArray *)getAllCurrencyList;

@end




























////
////  DBManager.h
////  Currency
////
////  Created by lijingjie on 15/4/18.
////  Copyright (c) 2015年 lijingjie. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//#import "FMDB.h"
//
//typedef void (^SuccessBlock)(id respondObject);
//typedef void (^FailureBlock)(NSError *error);
//
//@class Currency;
//
//@interface DBManager : NSObject
//
//+ (DBManager *)getInstance;
//
//@property FMDatabaseQueue *queue;
//
//#pragma mark - Currency
//- (void)addCurrency:(Currency *)currency;
//- (void)updateCurrency:(Currency *)currency;
//- (void)setShownOfCurrency:(Currency *)currency shown:(BOOL)isShown;
//- (void)setCurrency:(Currency *)currency toIndex:(int)index;
//- (NSMutableArray *)getShownCurrencyList;
//- (NSMutableArray *)getAllCurrencyList;
//- (int)getCurrencyCount;
//
//@end
