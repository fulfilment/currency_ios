//
//  DBManager.m
//  Rate_ios
//
//  Created by lichangyuan on 16/1/28.
//  Copyright © 2016年 lijingjie. All rights reserved.
//

#import "DBManager.h"
#import <sqlite3.h>
#import "Constants.h"

#define DATABASE_NAME @"currency.db"

@interface DBManager ()

@end

@implementation DBManager {
    FMDatabase *database;
}

static DBManager * dbManager;
+ (DBManager *)getInstance {
    if (!dbManager) {
        @synchronized (self) {
            if (!dbManager) {
                dbManager = [[DBManager alloc] init];
            }
        }
    }
    return dbManager;
}

- (id)init {
    self = [super init];
    if(self) {
        database = [FMDatabase databaseWithPath:[DBManager getDatabasePath]];
        [self createTables];
    }
    return self;
}

+ (void)migrateData {
    NSString *newPath = [self getDatabasePath];
    NSString *oldPath = [self getOldDatabasePath];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    if (![fileManager fileExistsAtPath:newPath] && [fileManager fileExistsAtPath:oldPath]) {
        [fileManager copyItemAtPath:oldPath toPath:newPath error:&error];
        if (error!=nil) {
            NSLog(@"%@", [error userInfo]);
        }
    }
}

#pragma mark - Currency
- (void)createTables {
    NSString *createCurrencyTable = @"CREATE TABLE IF NOT EXISTS tbl_currency (\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    name TEXT DEFAULT(''),\
    code TEXT DEFAULT(''),\
    symbol TEXT DEFAULT(''),\
    flag TEXT DEFAULT(''),\
    middle FLOAT DEFAULT(0),\
    buying FLOAT DEFAULT(0),\
    selling FLOAT DEFAULT(0),\
    cash_buying FLOAT DEFAULT(0),\
    cash_selling FLOAT DEFAULT(0),\
    show INT DEFAULT(1),\
    show_index INT DEFAULT(0))";
    [self executeCommand:createCurrencyTable withOperation:YES];
    
    [self initData];
}

- (void)initData {
    int count = [self getCurrencyCount];
    if (count == 0) {
        [self insertCurrency:[[Currency alloc] initWithName:@"美元" code:@"USD" symbol:@"$" flag:@"flag_usd" showIndex:0]];
        [self insertCurrency:[[Currency alloc] initWithName:@"欧元" code:@"EUR" symbol:@"€" flag:@"flag_eur" showIndex:1]];
        [self insertCurrency:[[Currency alloc] initWithName:@"英镑" code:@"GBP" symbol:@"£" flag:@"flag_gbp" showIndex:2]];
        [self insertCurrency:[[Currency alloc] initWithName:@"港币" code:@"HKD" symbol:@"$" flag:@"flag_hkd" showIndex:3]];
        [self insertCurrency:[[Currency alloc] initWithName:@"澳门元" code:@"MOP" symbol:@"$" flag:@"flag_mop" showIndex:4]];
        [self insertCurrency:[[Currency alloc] initWithName:@"新台币" code:@"TWD" symbol:@"$" flag:@"flag_twd" showIndex:5]];
        [self insertCurrency:[[Currency alloc] initWithName:@"日元" code:@"JPY" symbol:@"￥" flag:@"flag_jpy" showIndex:6]];
        [self insertCurrency:[[Currency alloc] initWithName:@"韩国元" code:@"KRW" symbol:@"₩" flag:@"flag_krw" showIndex:7]];
        [self insertCurrency:[[Currency alloc] initWithName:@"卢布" code:@"RUB" symbol:@"₽" flag:@"flag_rub" showIndex:8]];
        [self insertCurrency:[[Currency alloc] initWithName:@"澳大利亚元" code:@"AUD" symbol:@"$" flag:@"flag_aud" showIndex:9]];
        [self insertCurrency:[[Currency alloc] initWithName:@"新西兰元" code:@"NZD" symbol:@"$" flag:@"flag_nzd" showIndex:10]];
        [self insertCurrency:[[Currency alloc] initWithName:@"加拿大元" code:@"CAD" symbol:@"$" flag:@"flag_cad" showIndex:11]];
        [self insertCurrency:[[Currency alloc] initWithName:@"瑞士法郎" code:@"CHF" symbol:@"₣" flag:@"flag_chf" showIndex:12]];
        [self insertCurrency:[[Currency alloc] initWithName:@"丹麦克朗" code:@"DKK" symbol:@"Kr" flag:@"flag_dkk" showIndex:13]];
        [self insertCurrency:[[Currency alloc] initWithName:@"挪威克朗" code:@"NOK" symbol:@"Kr" flag:@"flag_nok" showIndex:14]];
        [self insertCurrency:[[Currency alloc] initWithName:@"瑞典克朗" code:@"SEK" symbol:@"Kr" flag:@"flag_sek" showIndex:15]];
        [self insertCurrency:[[Currency alloc] initWithName:@"新加坡元" code:@"SGD" symbol:@"$" flag:@"flag_sgd" showIndex:16]];
        [self insertCurrency:[[Currency alloc] initWithName:@"泰国铢" code:@"THB" symbol:@"฿" flag:@"flag_thb" showIndex:17]];
        [self insertCurrency:[[Currency alloc] initWithName:@"巴西里亚尔" code:@"BRL" symbol:@"$" flag:@"flag_brl" showIndex:18]];
        [self insertCurrency:[[Currency alloc] initWithName:@"印尼卢比" code:@"IDR" symbol:@"Rps" flag:@"flag_idr" showIndex:19]];
        [self insertCurrency:[[Currency alloc] initWithName:@"林吉特" code:@"MYR" symbol:@"$" flag:@"flag_myr" showIndex:20]];
        [self insertCurrency:[[Currency alloc] initWithName:@"菲律宾比索" code:@"PHP" symbol:@"₱" flag:@"flag_php" showIndex:21]];
        [self insertCurrency:[[Currency alloc] initWithName:@"阿联酋迪拉姆" code:@"AED" symbol:@"د.إ" flag:@"flag_aed" showIndex:22]];
        [self insertCurrency:[[Currency alloc] initWithName:@"印度卢比" code:@"INR" symbol:@"₹" flag:@"flag_inr" showIndex:23]];
        [self insertCurrency:[[Currency alloc] initWithName:@"南非兰特" code:@"ZAR" symbol:@"R" flag:@"flag_zar" showIndex:24]];
    }
    if (count == 25) {
        [self updateCurrencyCode:@"KER" newCode:@"KRW"];
        
        [self updateCurrencyFlag:@"CHF" flag:@"flag_chf"];
        [self updateCurrencyFlag:@"DKK" flag:@"flag_dkk"];
        [self updateCurrencyFlag:@"NOK" flag:@"flag_nok"];
        [self updateCurrencyFlag:@"SEK" flag:@"flag_sek"];
        [self updateCurrencyFlag:@"SGD" flag:@"flag_sgd"];
        [self updateCurrencyFlag:@"THB" flag:@"flag_thb"];
        [self updateCurrencyFlag:@"BRL" flag:@"flag_brl"];
        [self updateCurrencyFlag:@"IDR" flag:@"flag_idr"];
        [self updateCurrencyFlag:@"MYR" flag:@"flag_myr"];
        [self updateCurrencyFlag:@"PHP" flag:@"flag_php"];
        [self updateCurrencyFlag:@"KRW" flag:@"flag_krw"];
    }
    if (count < 27) {
        [self updateCurrencySymbol:@"RUB" symbol:@"₽"];
        
        [self insertCurrency:[[Currency alloc] initWithName:@"沙特里亚尔" code:@"SAR" symbol:@"ر.س" flag:@"flag_sar" showIndex:25]];
        [self insertCurrency:[[Currency alloc] initWithName:@"土耳其里拉" code:@"TRY" symbol:@"₤" flag:@"flag_try" showIndex:26]];
    }
}

- (void)insertCurrency:(Currency *)currency {
    NSString *command = [NSString stringWithFormat:@"INSERT INTO tbl_currency (name, code, symbol, flag, show, show_index, middle, buying, selling, cash_buying, cash_selling) VALUES ('%@','%@','%@', '%@','%d','%lu','%f','%f','%f','%f','%f')",
                         currency.name, currency.code, currency.symbol, currency.flag, currency.isShown, (unsigned long)currency.showIndex, currency.middleRate, currency.buyingRate, currency.sellingRate, currency.cashBuyingRate, currency.cashSellingRate];
    [self executeCommand:command withOperation:YES];
}

- (void)updateCurrency:(Currency *)currency {
    NSString *command = [NSString stringWithFormat:@"UPDATE tbl_currency SET middle = '%f', buying = '%f', selling = '%f', cash_buying = '%f', cash_selling = '%f' WHERE name = '%@'", currency.middleRate, currency.buyingRate, currency.sellingRate, currency.cashBuyingRate, currency.cashSellingRate, currency.name];
    [self executeCommand:command withOperation:YES];
}

- (void)updateCurrencyShowInfo:(Currency *)currency {
    int isShown = currency.isShown ? 1 : 0;
    NSString *command = [NSString stringWithFormat:@"UPDATE tbl_currency SET show = '%d', show_index = '%d' WHERE name = '%@'", isShown, currency.showIndex, currency.name];
    [self executeCommand:command withOperation:YES];
}

- (void)updateCurrencyCode:(NSString *)code newCode:(NSString *)newCode {
    NSString *command = [NSString stringWithFormat:@"UPDATE tbl_currency SET code = '%@' WHERE code = '%@'", newCode, code];
    [self executeCommand:command withOperation:YES];
}

- (void)updateCurrencySymbol:(NSString *)code symbol:(NSString *)symbol {
    NSString *command = [NSString stringWithFormat:@"UPDATE tbl_currency SET symbol = '%@' WHERE code = '%@'", symbol, code];
    [self executeCommand:command withOperation:YES];
}

- (void)updateCurrencyFlag:(NSString *)code flag:(NSString *)flag {
    NSString *command = [NSString stringWithFormat:@"UPDATE tbl_currency SET flag = '%@' WHERE code = '%@'", flag, code];
    [self executeCommand:command withOperation:YES];
}

- (int)getCurrencyCount {
    [database open];
    
    NSString *command = @"SELECT COUNT(DISTINCT(id)) AS count FROM tbl_currency";
    FMResultSet *resultSet = [database executeQuery:command];
    int count = 0;
    if ([resultSet next]) {
        count = [resultSet intForColumn:@"count"];
    }
    
    [database close];
    
    return count;
}

- (Currency *)getCurrencyByName:(NSString *)name {
    [database open];
    
    NSString *command = [NSString stringWithFormat:@"SELECT * FROM tbl_currency WHERE name = '%@'", name];
    FMResultSet *resultSet = [database executeQuery:command];
    Currency *currency;
    if ([resultSet next]) {
        currency = [self getCurrencyFromResultSet:resultSet];
    }
    
    [database close];
    
    return currency;
}

- (Currency *)getCurrencyByCode:(NSString *)code {
    [database open];
    
    NSString *command = [NSString stringWithFormat:@"SELECT * FROM tbl_currency WHERE code = '%@'", code];
    FMResultSet *resultSet = [database executeQuery:command];
    Currency *currency;
    if ([resultSet next]) {
        currency = [self getCurrencyFromResultSet:resultSet];
    }
    
    [database close];
    
    return currency;
}

- (NSMutableArray *)getAllCurrencyList {
    [database open];
    
    NSString *command = @"SELECT * FROM tbl_currency ORDER BY show_index";
    FMResultSet *resultSet = [database executeQuery:command];
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    while ([resultSet next]) {
        [resultArray addObject:[self getCurrencyFromResultSet:resultSet]];
    }
    
    [database close];
    
    return resultArray;
}

- (NSMutableArray *)getShownCurrencyList {
    [database open];
    
    NSString *command = @"SELECT * FROM tbl_currency WHERE show = 1 ORDER BY show_index";
    FMResultSet *resultSet = [database executeQuery:command];
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    while ([resultSet next]) {
        [resultArray addObject:[self getCurrencyFromResultSet:resultSet]];
    }
    
    [database close];
    
    return resultArray;
}

# pragma mark Auxiliary
+ (NSString *)getDatabasePath {
    NSURL *directory = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAppGroupId];
    return [[directory URLByAppendingPathComponent:DATABASE_NAME] path];
}

+ (NSString *)getOldDatabasePath {
    NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
   return [[documentsPaths objectAtIndex:0] stringByAppendingPathComponent:DATABASE_NAME];
}

- (void)executeCommand:(NSString *)command withOperation:(BOOL)withOperation {
    if (withOperation) {
        [database open];
    }
    
    [database executeUpdate:command];
    
    if (withOperation) {
        [database close];
    }
}

- (Currency *)getCurrencyFromResultSet:(FMResultSet *)resultSet {
    Currency *currency = [[Currency alloc] init];
    currency.name = [resultSet stringForColumn:@"name"];
    currency.code = [resultSet stringForColumn:@"code"];
    currency.symbol = [resultSet stringForColumn:@"symbol"];
    currency.flag = [resultSet stringForColumn:@"flag"];
    currency.middleRate = [resultSet doubleForColumn:@"middle"];
    currency.buyingRate = [resultSet doubleForColumn:@"buying"];
    currency.sellingRate = [resultSet doubleForColumn:@"selling"];
    currency.cashBuyingRate = [resultSet doubleForColumn:@"cash_buying"];
    currency.cashSellingRate = [resultSet doubleForColumn:@"cash_selling"];
    currency.isShown = [resultSet intForColumn:@"show"] > 0;
    currency.showIndex = [resultSet intForColumn:@"show_index"];
    return currency;
}

@end
