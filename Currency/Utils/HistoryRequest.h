//
//  HistoryRequest.h
//  Currency
//
//  Created by Tianyu An on 2017/3/19.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HistoryRequest : NSObject

+ (void)getRateByStartDate:(NSString *)startDate endDate:(NSString *)endDate name:(NSString *)name page:(int)page success:(void (^)(NSMutableArray *results))success failure:(void (^)())failure;

@end
