//
//  Constants.h
//  Currency
//
//  Created by Tianyu An on 2017/1/12.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kAppGroupId;

@interface Constants : NSObject

@end
