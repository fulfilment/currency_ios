//
//  CurrencyDefaults.m
//  Currency
//
//  Created by Tianyu An on 2017/1/15.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import "CurrencyDefaults.h"
#import "Constants.h"

#define KEY_LAST_UPDATE_TIME @"lastUpdateTime"
#define KEY_EMPHASIZE_TYPE @"emphasizeType"
#define KEY_EMPHASIZE_NAME @"emphasizeName"
#define KEY_EMPHASIZE_CODE @"emphasizeCode"
#define KEY_DRAG_HINT_SHOWN @"dragHintShown"

@implementation CurrencyDefaults {
    NSUserDefaults *userDefaults;
}

static CurrencyDefaults *defaults;
+ (CurrencyDefaults *)getInstance {
    if (!defaults) {
        @synchronized (self) {
            if (!defaults) {
                defaults = [[CurrencyDefaults alloc] init];
            }
        }
    }
    return defaults;
}

- (id)init {
    self = [super init];
    if(self) {
        userDefaults = [[NSUserDefaults alloc] initWithSuiteName:kAppGroupId];
    }
    return self;
}

- (void)initData {
    NSString *name = [defaults getEmphasizeName];
    if (name == nil) {
        [defaults setEmphasizeName:@"美元"];
        [defaults setEmphasizeCode:@"USD"];
        [defaults synchronize];
    }
}

- (void)synchronize {
    [userDefaults synchronize];
}

- (int)getLastUpdateTime {
    return (int) [userDefaults integerForKey:KEY_LAST_UPDATE_TIME];
}

- (int)getEmphasizeType {
    return (int) [userDefaults integerForKey:KEY_EMPHASIZE_TYPE];
}

- (NSString *)getEmphasizeName {
    return [userDefaults stringForKey:KEY_EMPHASIZE_NAME];
}

- (NSString *)getEmphasizeCode {
    return [userDefaults stringForKey:KEY_EMPHASIZE_CODE];
}

- (BOOL)isDragHintShown {
    return [userDefaults boolForKey:KEY_DRAG_HINT_SHOWN];
}

- (void)setLastUpdateTime:(NSInteger)lastUpdateTime {
    [userDefaults setInteger:lastUpdateTime forKey:KEY_LAST_UPDATE_TIME];
}

- (void)setEmphasizeType:(NSInteger)emphasizeType {
    [userDefaults setInteger:emphasizeType forKey:KEY_EMPHASIZE_TYPE];
}

- (void)setEmphasizeName:(NSString *)name {
    [userDefaults setValue:name forKey:KEY_EMPHASIZE_NAME];
}

- (void)setEmphasizeCode:(NSString *)code {
    [userDefaults setValue:code forKey:KEY_EMPHASIZE_CODE];
}

- (void)setDragHintShown {
    [userDefaults setBool:YES forKey:KEY_DRAG_HINT_SHOWN];
}

@end
