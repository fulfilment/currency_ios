//
//  CurrencyRequest.h
//  Currency
//
//  Created by Tianyu An on 2017/3/12.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrencyRequest : NSObject

+ (void)getRate:(void (^)(NSMutableArray *results))success failure:(void (^)())failure;

@end
