//
//  CurrencyDefaults.h
//  Currency
//
//  Created by Tianyu An on 2017/1/15.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrencyDefaults : NSObject

+ (CurrencyDefaults *)getInstance;

- (void)initData;
- (void)synchronize;

- (int)getLastUpdateTime;
- (int)getEmphasizeType;
- (NSString *)getEmphasizeName;
- (NSString *)getEmphasizeCode;
- (BOOL)isDragHintShown;

- (void)setLastUpdateTime:(NSInteger)lastUpdateTime;
- (void)setEmphasizeType:(NSInteger)emphasizeType;
- (void)setEmphasizeName:(NSString *)name;
- (void)setEmphasizeCode:(NSString *)code;
- (void)setDragHintShown;

@end
