//
//  ViewUtils.m
//  AccountCleaner
//
//  Created by Tianyu An on 15/11/24.
//  Copyright © 2015年 Tianyu An. All rights reserved.
//

#import "ViewUtils.h"

@implementation ViewUtils

+ (void)pushViewController:(UIViewController *)controller target:(UIViewController *)target {
    target.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [target.navigationController pushViewController:controller animated:YES];
}

+ (void)showToast:(UIViewController *)controller content:(NSString *)content {
    [controller.view hideToast];
    [controller.view makeToast:content duration:1.5 position:@"center"];
}

+ (void)showToastInView:(UIView *)view content:(NSString *)content {
    [view hideToast];
    [view makeToast:content duration:1.5 position:@"center"];
}

+ (UIView *)getLayout:(UIViewController *)controller title:(NSString *)title positionY:(double)positionY {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, positionY, controller.view.frame.size.width, LAYOUT_HEIGHT)];
    view.backgroundColor = [UIColor whiteColor];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 12, 200, 24)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:16];
    label.text = title;
    [view addSubview:label];
    
    return view;
}

+ (UIView *)getDividerInPositionY:(double)positionY {
    UIView *divider = [[UIView alloc] init];
    divider.frame = CGRectMake(0, positionY, PHONE_WIDTH, DIVIDER_HEIGHT);
    divider.backgroundColor = UIColorFromRGB(0xD0D0D0);
    return divider;
}

+ (void)setRateLabel:(UILabel *)label rate:(CGFloat)rate {
    label.text = rate == 0 ? @"    -" : [NSString stringWithFormat:@"¥%.2f", rate];
}

+ (NSString *)getInfoString:(Currency *)currency {
    return [NSString stringWithFormat:@"%@ %@(%@)", currency.name, currency.code, currency.symbol];
}

+ (NSString *)timestampToString:(NSInteger)timestamp {
    if (timestamp == 0) {
        return @"-";
    } else {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        return [formatter stringFromDate:date];
    }
}

+ (NSString *)getCurrentTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    return [formatter stringFromDate:[NSDate date]];
}

+ (double)stringToAmount:(NSString *)source {
    return round([source doubleValue] * 100) / 100;
}

+ (NSString *)formatAmount:(double)source {
    return [NSString stringWithFormat:@"%.2f", source];
}

+ (NSString *)removeDoubleRedundancy:(NSString *)amount {
    int index = (int) (amount.length - 1);
    while ([amount characterAtIndex:index] == '0') {
        index--;
    }
    if ([amount characterAtIndex:index] == '.') {
        index--;
    }
    return [[amount substringToIndex:index + 1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

@end
