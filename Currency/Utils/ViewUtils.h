//
//  ViewUtils.h
//  AccountCleaner
//
//  Created by Tianyu An on 15/11/24.
//  Copyright © 2015年 Tianyu An. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Toast+UIView.h"
#import "Currency.h"

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define PHONE_WIDTH [[UIScreen mainScreen] bounds].size.width
#define PHONE_HEIGHT [[UIScreen mainScreen] bounds].size.height

#define MAJOR_COLOR UIColorFromRGB(0x49516F)
#define MINORITY_COLOR UIColorFromRGB(0xA9A9A9)
#define BACKGROUND_COLOR UIColorFromRGB(0xEDEDEF)
#define MARKED_COLOR UIColorFromRGB(0xEF476F)

#define DIVIDER_HEIGHT 0.5
#define LAYOUT_HEIGHT 48

@interface ViewUtils : NSObject

+ (void)pushViewController:(UIViewController *)controller target:(UIViewController *)target;
+ (void)showToast:(UIViewController *)controller content:(NSString *)content;
+ (void)showToastInView:(UIView *)view content:(NSString *)content;
+ (UIView *)getLayout:(UIViewController *)controller title:(NSString *)title positionY:(double)positionY;
+ (UIView *)getDividerInPositionY:(double)positionY;
+ (void)setRateLabel:(UILabel *)label rate:(CGFloat)rate;

+ (NSString *)getInfoString:(Currency *)currency;
+ (NSString *)timestampToString:(NSInteger)timestamp;
+ (NSString *)getCurrentTime;
+ (NSString *)formatAmount:(double)source;

@end
