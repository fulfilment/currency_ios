//
//  CurrencyRequest.m
//  Currency
//
//  Created by Tianyu An on 2017/3/12.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import "CurrencyRequest.h"
#import "AFNetworking.h"
#import "HTMLReader.h"
#import "Currency.h"

@implementation CurrencyRequest

+ (void)getRate:(void (^)(NSMutableArray *results))success failure:(void (^)())failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:@"http://www.boc.cn/sourcedb/whpj/" parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *results = [[NSMutableArray alloc] init];
        
        @try {
            NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            
            HTMLDocument *document = [HTMLDocument documentWithString:responseString];
            HTMLElement *table = [[document nodesMatchingSelector:@"table"] objectAtIndex:1];
            NSArray<HTMLElement *> *tableRows = [table nodesMatchingSelector:@"tr"];
            for (int i = 1; i < tableRows.count; i++) {
                NSArray<HTMLElement *> *tds = [[tableRows objectAtIndex:i] nodesMatchingSelector:@"td"];
                
                NSString *name = [tds objectAtIndex:0].textContent;
                CGFloat middleRate = [[tds objectAtIndex:5].textContent floatValue];
                CGFloat buyingRate = [[tds objectAtIndex:1].textContent floatValue];
                CGFloat sellingRate = [[tds objectAtIndex:3].textContent floatValue];
                CGFloat cashBuyingRate = [[tds objectAtIndex:2].textContent floatValue];
                CGFloat cashSellingRate = [[tds objectAtIndex:4].textContent floatValue];
                
                [results addObject:[[Currency alloc] initWithName:name
                                                           middle:middleRate
                                                           buying:buyingRate
                                                          selling:sellingRate
                                                       cashBuying:cashBuyingRate
                                                      cashSelling:cashSellingRate]];
            }
            
            if (success) {
                success(results);
            }
        } @catch (NSException *exception) {
            if (success) {
                success(results);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure();
        }
    }];
}

@end
