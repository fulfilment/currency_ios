//
//  HistoryRequest.m
//  Currency
//
//  Created by Tianyu An on 2017/3/19.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import "HistoryRequest.h"
#import "AFNetworking.h"
#import "Currency.h"
#import "HTMLReader.h"

@implementation HistoryRequest

+ (void)getRateByStartDate:(NSString *)startDate endDate:(NSString *)endDate name:(NSString *)name page:(int)page success:(void (^)(NSMutableArray *results))success failure:(void (^)())failure {
    NSString *url = @"https://srh.bankofchina.com/search/whpj/search_cn.jsp";
    
    NSDictionary *params = @{@"erectDate":startDate,
                             @"nothing":endDate,
                             @"pjname":name,
                             @"page":[NSNumber numberWithInt:page]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *results = [[NSMutableArray alloc] init];
        
        @try {
            NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            
            HTMLDocument *document = [HTMLDocument documentWithString:responseString];
            HTMLNode *table = [[document firstNodeMatchingSelector:@".BOC_main"] childAtIndex:1];
            NSArray<HTMLElement *> *tableRows = [table nodesMatchingSelector:@"tr"];
            
            HTMLElement *element = [[tableRows objectAtIndex:1] firstNodeMatchingSelector:@"th"];
            if ([element.textContent containsString:@"对不起"] && success) {
                success(results);
                return;
            }
            
            for (int i = 1; i < tableRows.count; i++) {
                NSArray<HTMLElement *> *tds = [[tableRows objectAtIndex:i] nodesMatchingSelector:@"td"];
                
                if (tds.count <= 1) {
                    continue;
                }
                
                CGFloat middleRate = [[tds objectAtIndex:5].textContent floatValue];
                CGFloat buyingRate = [[tds objectAtIndex:1].textContent floatValue];
                CGFloat sellingRate = [[tds objectAtIndex:3].textContent floatValue];
                CGFloat cashBuyingRate = [[tds objectAtIndex:2].textContent floatValue];
                CGFloat cashSellingRate = [[tds objectAtIndex:4].textContent floatValue];
                NSString *updateTime = [[tds objectAtIndex:6].textContent stringByReplacingOccurrencesOfString:@"." withString:@"-"];
                
                [results addObject:[[Currency alloc] initWithUpdateTime:updateTime
                                                                 middle:middleRate
                                                                 buying:buyingRate
                                                                selling:sellingRate
                                                             cashBuying:cashBuyingRate
                                                            cashSelling:cashSellingRate]];
            }
            
            if (success) {
                success(results);
            }
        } @catch (NSException *exception) {
            if (success) {
                success(results);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure();
        }
    }];
}

@end
