//
//  Constants.m
//  Currency
//
//  Created by Tianyu An on 2017/1/12.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import "Constants.h"

NSString *const kAppGroupId = @"group.com.antianyu.currency";

@implementation Constants

@end
