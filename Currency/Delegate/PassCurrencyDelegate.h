//
//  PassCurrencyDelegate.h
//  Currency
//
//  Created by Tianyu An on 2017/4/23.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Currency.h"

@protocol PassCurrencyDelegate <NSObject>

- (void)passCurrency:(Currency *)chosenCurrency;

@end
