//
//  AboutViewController.m
//  AccountCleaner
//
//  Created by Tianyu An on 14-5-28.
//  Copyright (c) 2014年 Tianyu An. All rights reserved.
//

#import "AboutViewController.h"
#import "Masonry.h"
#import "ChatLabel.h"
#import "ViewUtils.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

#pragma mark - View
- (void)initView {
    [super initView];
    
    self.title = @"关于";
    
    NSString * versionCode = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    self.versionLabel.text = [NSString stringWithFormat:@"v%@", versionCode];
    
    ChatLabel *secondLabel = [[ChatLabel alloc] initWithFrame:CGRectZero type:ChatLabelTypeOthers];
    secondLabel.contentLabel.text = @"数据来源：中国银行 http://www.boc.cn/sourcedb/whpj";
    [self.view addSubview:secondLabel];
    
    [secondLabel addHorizontalContraints];
    [secondLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view.mas_bottom).offset(-40);
    }];
    
    ChatLabel *firstLabel = [[ChatLabel alloc] initWithFrame:CGRectZero type:ChatLabelTypeOthers];
    firstLabel.contentLabel.text = @"如遇到任何问题或者有好的意见建议请发送至aty_3361@sina.com！谢谢！";
    [self.view addSubview:firstLabel];
    
    [firstLabel addHorizontalContraints];
    [firstLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(secondLabel.mas_top).offset(-20);
    }];
    
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.text = @"Tianyu An:";
    nameLabel.textColor = MINORITY_COLOR;
    nameLabel.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:nameLabel];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(30);
        make.bottom.equalTo(firstLabel.mas_top).offset(-10);
    }];
}

@end
