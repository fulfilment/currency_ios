//
//  WidgetTableViewController.h
//  Currency
//
//  Created by Tianyu An on 2017/1/7.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

@interface WidgetTableViewController : BaseTableViewController

@end
