//
//  MainTableViewController.m
//  Currency
//
//  Created by lichangyuan on 16/1/29.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import "MainTableViewController.h"
#import "ExchangeViewController.h"
#import "HistoryPrepareViewController.h"
#import "SettingsViewController.h"
#import "AboutViewController.h"
#import "InfoTableViewCell.h"
#import "MainTableViewCell.h"
#import "ViewUtils.h"
#import "SVProgressHUD.h"
#import "DBManager.h"
#import "CurrencyDefaults.h"
#import "Currency.h"
#import "CurrencyRequest.h"

@interface MainTableViewController () {
    DBManager *dbManager;
    CurrencyDefaults *defaults;
    NSString *cellIdentifier;
    NSArray *currencyList;
    BOOL hasInit;
}

@end

@implementation MainTableViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
    if (!hasInit) {
        [self updateCurrencyDataBase];
    }
}

- (void)initData {
    defaults = [CurrencyDefaults getInstance];
    dbManager = [DBManager getInstance];
    hasInit = NO;
}

- (void)initView {
    UIBarButtonItem *moreButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"More"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(moreClicked)];
    self.navigationItem.leftBarButtonItem = moreButton;
    
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Refresh"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(refreshData)];
    self.navigationItem.rightBarButtonItem = refreshButton;
}

- (void)loadData {
    int type = [defaults getEmphasizeType];
    switch (type) {
        case TYPE_CASH_SELLING:
            cellIdentifier = @"CashSellingCellIdentifier";
            break;
        case TYPE_CASH_BUYING:
            cellIdentifier = @"CashBuyingCellIdentifier";
            break;
        case TYPE_SELLING:
            cellIdentifier = @"SellingCellIdentifier";
            break;
        case TYPE_BUYING:
            cellIdentifier = @"BuyingCellIdentifier";
            break;
        default:
            cellIdentifier = @"MiddleCellIdentifier";
            break;
    }
    
    currencyList = [NSMutableArray arrayWithArray:[dbManager getShownCurrencyList]];
    [self.tableView reloadData];
}

- (void)refreshData {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD show];
    [self updateCurrencyDataBase];
}

- (void)updateCurrencyDataBase {
    [CurrencyRequest getRate:^(NSMutableArray *results) {
        for (Currency *currency in results) {
            [dbManager updateCurrency:currency];
        }
        
        [defaults setLastUpdateTime:[[NSDate date] timeIntervalSince1970]];
        [defaults synchronize];
        
        hasInit = YES;
        [SVProgressHUD dismiss];
        [self loadData];
    } failure:^{
        hasInit = YES;
        [ViewUtils showToast:self content:@"更新数据失败"];
        [SVProgressHUD dismiss];
    }];
}

- (void)moreClicked {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil
                                                                         message:nil
                                                                  preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[self buildAlertActionWithTitle:@"历史汇率" controller:[[HistoryPrepareViewController alloc] init]]];
    [actionSheet addAction:[self buildAlertActionWithTitle:@"设置" controller:[[SettingsViewController alloc] init]]];
    [actionSheet addAction:[self buildAlertActionWithTitle:@"关于" controller:[[AboutViewController alloc] init]]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"取消"
                                                    style:UIAlertActionStyleCancel
                                                  handler:nil]];
    
    [self presentViewController:actionSheet animated:true completion:nil];
}

- (UIAlertAction *)buildAlertActionWithTitle:(NSString *)title controller:(UIViewController *)controller {
    return [UIAlertAction actionWithTitle:title
                                    style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * _Nonnull action) {
        [ViewUtils pushViewController:controller target:self];
    }];
}

#pragma mark - UITableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        InfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InfoCellIdentifier" forIndexPath:indexPath];
        [cell setTime:[ViewUtils timestampToString:[defaults getLastUpdateTime]]];
        return cell;
    } else {
        MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        [cell setCurrency:[currencyList objectAtIndex:indexPath.row - 1]];
        return cell;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return currencyList.count + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.row == 0 ? 52 : 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row > 0) {
        ExchangeViewController *controller = [[ExchangeViewController alloc] init];
        controller.currency = [currencyList objectAtIndex:indexPath.row - 1];
        [ViewUtils pushViewController:controller target:self];
    }
}

@end
