//
//  HistoryPrepareViewController.h
//  Currency
//
//  Created by Tianyu An on 2017/4/9.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "PassCurrencyDelegate.h"

@interface HistoryPrepareViewController : BaseViewController<PassCurrencyDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *flagImageView;
@property (strong, nonatomic) IBOutlet UILabel *currencyLabel;
@property (strong, nonatomic) IBOutlet UILabel *startLabel;
@property (strong, nonatomic) IBOutlet UILabel *endLabel;
@property (strong, nonatomic) IBOutlet UIControl *pickerLayout;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

- (IBAction)currencyViewClicked:(id)sender;
- (IBAction)startViewClicked:(id)sender;
- (IBAction)endViewClicked:(id)sender;
- (IBAction)historyButtonClicked:(id)sender;
- (IBAction)confirmButtonClicked:(id)sender;
- (IBAction)dismissDatePicker:(id)sender;

@end
