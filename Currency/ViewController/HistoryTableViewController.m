//
//  HistoryTableViewController.m
//  Currency
//
//  Created by Tianyu An on 2017/4/23.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import "HistoryTableViewController.h"
#import "HistoryTableViewCell.h"
#import "ExchangeViewController.h"
#import "ViewUtils.h"
#import "MJRefresh.h"
#import "SVProgressHUD.h"
#import "HistoryRequest.h"

#define HISTORY_PAGE_SIZE 20;

@interface HistoryTableViewController () {
    NSMutableArray *currencyList;
    int pageIndex;
    BOOL noMoreData;
}

@end

@implementation HistoryTableViewController

@synthesize chosenCurrency;
@synthesize startDate;
@synthesize endDate;

- (void)initData {
    currencyList = [[NSMutableArray alloc] init];
    pageIndex = 0;
    noMoreData = NO;
}

- (void)initView {
    self.title = [NSString stringWithFormat:@"历史汇率 (%@)", chosenCurrency.name];
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self sendHistoryRequest:1];
    }];
    header.lastUpdatedTimeLabel.hidden = YES;
    self.tableView.mj_header = header;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD show];
    [self sendHistoryRequest:pageIndex + 1];
}

- (void)sendHistoryRequest:(int)page {
    [HistoryRequest getRateByStartDate:startDate endDate:endDate name:chosenCurrency.name page:page success:^(NSMutableArray *results) {
        if (page == 1) {
            [currencyList removeAllObjects];
        }
        [currencyList addObjectsFromArray:results];
        if (results.count > 0) {
            pageIndex = page;
        }
        noMoreData = results.count < HISTORY_PAGE_SIZE;
        [self.tableView reloadData];
        
        [SVProgressHUD dismiss];
        [self setFooter];
        [self stopViewGetting];
    } failure:^{
        [ViewUtils showToast:self content:@"获取数据失败"];
        [self stopViewGetting];
    }];
}

- (void)setFooter {
    if (!self.tableView.mj_footer && !noMoreData) {
        self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            if (noMoreData) {
                [self.tableView.mj_footer endRefreshing];
            } else {
                [self sendHistoryRequest:pageIndex + 1];
            }
        }];
    }
}

- (void)stopViewGetting {
    [SVProgressHUD dismiss];
    [self.tableView.mj_header endRefreshing];
    if (noMoreData) {
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    } else {
        [self.tableView.mj_footer endRefreshing];
    }
}

#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (currencyList.count) {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        self.tableView.backgroundView = nil;
        return 1;
    } else {
        UILabel *label = [[UILabel alloc] initWithFrame:self.tableView.bounds];
        label.text = @"没有历史数据";
        label.font = [UIFont systemFontOfSize:14];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = UIColorFromRGB(0x9C9C9C);
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.backgroundView = label;
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 102;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return currencyList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCellIdentifier"];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:nil options:nil] lastObject];
    }
    
    [cell setCurrency:[currencyList objectAtIndex:indexPath.item]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ExchangeViewController *controller = [[ExchangeViewController alloc] init];
    Currency *currency = [currencyList objectAtIndex:indexPath.row];
    currency.flag = chosenCurrency.flag;
    currency.name = chosenCurrency.name;
    currency.code = chosenCurrency.code;
    currency.symbol = chosenCurrency.symbol;
    controller.currency = currency;
    [ViewUtils pushViewController:controller target:self];
}

@end
