//
//  CalculateViewController.m
//  Currency
//
//  Created by Tianyu An on 16/3/26.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import "ExchangeViewController.h"
#import "ViewUtils.h"
#import "CurrencyDefaults.h"

@interface ExchangeViewController () {
    double rate;
    BOOL convertFromCNY;
    BOOL manuallySet;
}

@end

@implementation ExchangeViewController

@synthesize rateLabel;
@synthesize flagImageView;
@synthesize nameLabel;
@synthesize symbolLabel;
@synthesize leftTextField;
@synthesize rightTextField;
@synthesize currency;

- (void)initData {
    int type = [[CurrencyDefaults getInstance] getEmphasizeType];
    if (type == TYPE_CASH_SELLING && currency.cashSellingRate > 0) {
        [self setRate:currency.cashSellingRate rateName:@"钞卖"];
    } else if (type == TYPE_CASH_BUYING && currency.cashBuyingRate > 0) {
        [self setRate:currency.cashBuyingRate rateName:@"钞买"];
    } else if (type == TYPE_SELLING && currency.sellingRate > 0) {
        [self setRate:currency.sellingRate rateName:@"汇卖"];
    } else if (type == TYPE_BUYING && currency.buyingRate > 0) {
        [self setRate:currency.buyingRate rateName:@"汇买"];
    } else {
        [self setRate:currency.middleRate rateName:@"中间价"];
    }
}

- (void)initView {
    self.title = @"换算";
    
    flagImageView.image = [UIImage imageNamed:currency.flag];
    nameLabel.text = currency.name;
    symbolLabel.text = currency.symbol;
    
    [leftTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [rightTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [leftTextField becomeFirstResponder];
}

- (IBAction)rateButtonClicked:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil
                                                                         message:nil
                                                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *middleButton = [UIAlertAction actionWithTitle:@"中间价"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                       [self setRate:currency.middleRate rateName:@"中间价"];
                                   }];
    [actionSheet addAction:middleButton];
    
    if (currency.cashSellingRate > 0) {
        UIAlertAction *button = [UIAlertAction actionWithTitle:@"现钞卖出价"
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * _Nonnull action) {
                                          [self setRate:currency.cashSellingRate rateName:@"钞卖"];
                                      }];
        [actionSheet addAction:button];
    }
    
    if (currency.cashBuyingRate > 0) {
        UIAlertAction *button = [UIAlertAction actionWithTitle:@"现钞买入价"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                     [self setRate:currency.cashBuyingRate rateName:@"钞买"];
                                 }];
        [actionSheet addAction:button];
    }
    
    if (currency.sellingRate > 0) {
        UIAlertAction *button = [UIAlertAction actionWithTitle:@"现汇卖出价"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                     [self setRate:currency.sellingRate rateName:@"汇卖"];
                                 }];
        [actionSheet addAction:button];
    }
    
    if (currency.buyingRate > 0) {
        UIAlertAction *button = [UIAlertAction actionWithTitle:@"现汇买入价"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                     [self setRate:currency.buyingRate rateName:@"汇买"];
                                 }];
        [actionSheet addAction:button];
    }
    
    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:@"取消"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    [actionSheet addAction:cancelButton];
    
    [self presentViewController:actionSheet animated:true completion:nil];
}

- (void)setRate:(double)newRate rateName:(NSString *)name {
    rate = newRate / 100;
    rateLabel.text = name;
    if ([leftTextField isFirstResponder]) {
        rightTextField.text = [ViewUtils formatAmount:[leftTextField.text doubleValue] / rate];
    } else if ([rightTextField isFirstResponder]) {
        leftTextField.text = [ViewUtils formatAmount:[rightTextField.text doubleValue] * rate];
    }
}

#pragma mark - UITextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSCharacterSet *characterSet = [[NSCharacterSet characterSetWithCharactersInString:@".1234567890"] invertedSet];
    NSString *filteredResult=[[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
    if (![string isEqualToString:filteredResult]) {
        return NO;
    }
    
    static BOOL containsDecPoint;
    NSUInteger pointIndex = [textField.text rangeOfString:@"."].location;
    if (pointIndex == NSNotFound) {
        containsDecPoint = NO;
    }
    if (string.length > 0) {
        unichar single = [string characterAtIndex:0];
        if(textField.text.length == 0 && single == '.') {
            textField.text = @"0";
            containsDecPoint = YES;
        } else if (textField.text.length > 0) {
            if(single == '.') {
                if (!containsDecPoint) {
                    containsDecPoint = YES;
                } else {
                    return NO;
                }
            } else if (pointIndex == textField.text.length - 3) {
                return NO;
            } else if ([textField.text isEqualToString:@"0"]) {
                if (single == '0') {
                    return NO;
                } else if (single != '.') {
                    textField.text = @"";
                }
            }
        }
    }
    
    convertFromCNY = textField == leftTextField;
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([textField.text isEqualToString:@"0.00"]) {
        textField.text = @"";
        if (textField == leftTextField) {
            rightTextField.text = @"0.00";
        } else {
            leftTextField.text = @"0.00";
        }
    }
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (textField == leftTextField && convertFromCNY && !manuallySet) {
        rightTextField.text = [NSString stringWithFormat:@"%.2f", [textField.text doubleValue] / rate];
    } else if (textField == rightTextField && !convertFromCNY && !manuallySet) {
        leftTextField.text = [NSString stringWithFormat:@"%.2f", [textField.text doubleValue] * rate];
    }
    manuallySet = NO;
}

@end
