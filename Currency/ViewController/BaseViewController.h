//
//  BaseViewController.h
//  Currency
//
//  Created by Tianyu An on 2017/3/12.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

- (void)initData;
- (void)initView;

@end
