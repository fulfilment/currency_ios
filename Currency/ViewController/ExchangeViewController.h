//
//  CalculateViewController.h
//  Currency
//
//  Created by Tianyu An on 16/3/26.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Currency.h"

@interface ExchangeViewController : BaseViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UILabel *rateLabel;
@property (strong, nonatomic) IBOutlet UIImageView *flagImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *symbolLabel;
@property (strong, nonatomic) IBOutlet UITextField *leftTextField;
@property (strong, nonatomic) IBOutlet UITextField *rightTextField;

- (IBAction)rateButtonClicked:(id)sender;

@property (strong, nonatomic) Currency *currency;

@end
