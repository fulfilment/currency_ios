//
//  HistoryTableViewController.h
//  Currency
//
//  Created by Tianyu An on 2017/4/23.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import "BaseTableViewController.h"
#import "Currency.h"

@interface HistoryTableViewController : BaseTableViewController

@property (nonatomic, strong) Currency *chosenCurrency;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *endDate;

@end
