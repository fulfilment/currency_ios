//
//  PickTableViewController.m
//  Currency
//
//  Created by Tianyu An on 2017/4/23.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import "PickTableViewController.h"
#import "SimpleTableViewCell.h"
#import "DBManager.h"
#import "CurrencyDefaults.h"

@interface PickTableViewController () {
    NSArray *currencyList;
}

@end

@implementation PickTableViewController

@synthesize chosenCurrency;

- (void)initData {
    currencyList = [NSMutableArray arrayWithArray:[[DBManager getInstance] getShownCurrencyList]];
}

- (void)initView {
    self.title = @"选择货币";
}

#pragma mark - UITableView
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return currencyList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SimpleCellIdentifier"];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SimpleTableViewCell" owner:nil options:nil] lastObject];
    }
    
    Currency *currency = [currencyList objectAtIndex:indexPath.item];
    [cell setCurrency:currency];
    
    if ([currency.code caseInsensitiveCompare:chosenCurrency.code] == NSOrderedSame) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.delegate passCurrency:[currencyList objectAtIndex:indexPath.row]];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
