//
//  ChooseUnitViewController.m
//  Currency
//
//  Created by Tianyu An on 16/3/28.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import "ChooseViewController.h"
#import "CurrencyCollectionCell.h"
#import "DBManager.h"
#import "ViewUtils.h"
#import "CurrencyDefaults.h"

@interface ChooseViewController () {
    DBManager *dbManager;
}

@end

@implementation ChooseViewController

@synthesize currencyList;

static NSString * const reuseIdentifier = @"CurrencyCellIdentifier";

- (instancetype)init {
    // 创建一个流水布局
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    
    // 设置cell的尺寸
    if (PHONE_WIDTH < 375) {
        layout.itemSize = CGSizeMake(151, 145);
    } else if (PHONE_WIDTH > 375) {
        layout.itemSize = CGSizeMake(130, 145);
    } else {
        layout.itemSize = CGSizeMake(117, 145);
    }
    
    // 设置滚动的方向
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    // 行间距
    layout.minimumLineSpacing = 6;
    
    // 设置cell之间的间距
    layout.minimumInteritemSpacing = 6;
    
    layout.sectionInset = UIEdgeInsetsMake(6, 6, 6, 6);
    
    return [super initWithCollectionViewLayout:layout];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"选择与排列货币";
    self.collectionView.backgroundColor = UIColorFromRGB(0xF5F6F8);
    
    UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonClicked)];
    self.navigationItem.rightBarButtonItem = saveButtonItem;
    
    UINib *cellNib = [UINib nibWithNibName:@"CurrencyCollectionCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:reuseIdentifier];
    
    UILongPressGestureRecognizer *longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    [self.collectionView addGestureRecognizer:longGesture];
    
    dbManager = [DBManager getInstance];
    currencyList = [dbManager getAllCurrencyList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (![[CurrencyDefaults getInstance] isDragHintShown]) {
        [[CurrencyDefaults getInstance] setDragHintShown];
        
        [self showDragHint];
    }
}

- (void)saveButtonClicked {
    for (int i = 0; i < currencyList.count; i++) {
        Currency *currency = [currencyList objectAtIndex:i];
        currency.showIndex = i;
        [dbManager updateCurrencyShowInfo:currency];
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - UICollectionView
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return currencyList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CurrencyCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CurrencyCollectionCell" owner:nil options:nil] lastObject];
    }

    Currency *currency = [self.currencyList objectAtIndex:indexPath.item];
    [cell setCurrency:currency];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CurrencyCollectionCell *cell = (CurrencyCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.coverView.hidden = !cell.coverView.hidden;
    
    Currency *currency = [currencyList objectAtIndex:indexPath.row];
    currency.isShown = !cell.coverView.hidden;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath*)destinationIndexPath {
    Currency *currency = [currencyList objectAtIndex:sourceIndexPath.item];
    [currencyList removeObject:currency];
    [currencyList insertObject:currency atIndex:destinationIndexPath.item];
}

- (void)handleLongGesture:(UILongPressGestureRecognizer *)longGesture {
    switch (longGesture.state) {
        case UIGestureRecognizerStateBegan: {
            NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:[longGesture locationInView:self.collectionView]];
            if (!indexPath) {
                [self.collectionView beginInteractiveMovementForItemAtIndexPath:indexPath];
            }
            break;
        }
        case UIGestureRecognizerStateChanged:
            [self.collectionView updateInteractiveMovementTargetPosition:[longGesture locationInView:self.collectionView]];
            break;
        case UIGestureRecognizerStateEnded:
            [self.collectionView endInteractiveMovement];
            break;
        default:
            [self.collectionView cancelInteractiveMovement];
            break;
    }
}

- (void)showDragHint {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil
                                                                        message:@"可以长按某个货币来拖动排序"
                                                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *button = [UIAlertAction actionWithTitle:@"知道了"
                                                     style:UIAlertActionStyleDefault
                                                   handler:nil];
    
    [controller addAction:button];
    [self presentViewController:controller animated:YES completion:nil];
}

@end
