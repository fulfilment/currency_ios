//
//  ChooseUnitViewController.h
//  Currency
//
//  Created by Tianyu An on 16/3/28.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseViewController : UICollectionViewController

@property (nonatomic, strong) NSMutableArray *currencyList;

@end
