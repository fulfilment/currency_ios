//
//  WidgetTableViewController.m
//  Currency
//
//  Created by Tianyu An on 2017/1/7.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import "WidgetTableViewController.h"
#import "SimpleTableViewCell.h"
#import "DBManager.h"
#import "CurrencyDefaults.h"

@interface WidgetTableViewController () {
    CurrencyDefaults *defaults;
    DBManager *dbManager;
    NSArray *currencyList;
    Currency *chosenCurrency;
}

@end

@implementation WidgetTableViewController

- (void)initData {
    defaults = [CurrencyDefaults getInstance];
    dbManager = [DBManager getInstance];
    currencyList = [NSMutableArray arrayWithArray:[dbManager getShownCurrencyList]];
    chosenCurrency = [Currency getCurrencyFromList:currencyList byCode:[defaults getEmphasizeCode]];
}

- (void)initView {
    self.title = @"选择Widget展示货币";
    
    UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonClicked)];
    self.navigationItem.rightBarButtonItem = saveButtonItem;
}

- (void)saveButtonClicked {
    [defaults setEmphasizeName:chosenCurrency.name];
    [defaults setEmphasizeCode:chosenCurrency.code];
    [defaults synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return currencyList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SimpleCellIdentifier"];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SimpleTableViewCell" owner:nil options:nil] lastObject];
    }
    
    Currency *currency = [currencyList objectAtIndex:indexPath.item];
    [cell setCurrency:currency];

    if ([currency.code caseInsensitiveCompare:chosenCurrency.code] == NSOrderedSame) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    chosenCurrency = [currencyList objectAtIndex:indexPath.row];
    [self.tableView reloadData];
}

@end
