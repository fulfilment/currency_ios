//
//  AboutViewController.h
//  AccountCleaner
//
//  Created by Tianyu An on 14-5-28.
//  Copyright (c) 2014年 Tianyu An. All rights reserved.
//

#import "BaseViewController.h"

@interface AboutViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UILabel *versionLabel;
@property (strong, nonatomic) IBOutlet UILabel *aboutLabel;

@end
