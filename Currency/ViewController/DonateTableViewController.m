//
//  DonateTableViewController.m
//  Currency
//
//  Created by Tianyu An on 2019/11/12.
//  Copyright © 2019 Tianyu An. All rights reserved.
//

#import "DonateTableViewController.h"
#import "SVProgressHUD.h"
#import "ViewUtils.h"

@interface DonateTableViewController () {
    NSArray<SKProduct *> *products;
    BOOL isProductQueried;
}

@end

@implementation DonateTableViewController

- (void)initView {
    self.title = @"捐赠";
}

- (void)initData {
    [super initData];
    products = [[NSArray alloc] init];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSSet *set = [[NSSet alloc] initWithObjects:@"com.antianyu.currency.donate6", @"com.antianyu.currency.donate12", nil];
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:set];
    request.delegate = self;
    [request start];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD show];
}

- (void)dealloc {
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

#pragma mark - IAP
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    products = [response.products sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"price" ascending:YES]]];
    [self.tableView reloadData];
    [SVProgressHUD show];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray<SKPaymentTransaction *> *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [SVProgressHUD showErrorWithStatus:@"支付失败"];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [SVProgressHUD showSuccessWithStatus:@"支付成功，感谢您的支持"];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            default:
                [SVProgressHUD dismiss];
                break;
        }
    }
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSString *productIdentifier = transaction.payment.productIdentifier;
    NSData *receiptData = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
    NSString *receipt = [receiptData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    if ([receipt length] > 0 && [productIdentifier length] > 0) {
        [SVProgressHUD showSuccessWithStatus:@"支付成功，感谢您的支持"];
    } else {
        [SVProgressHUD showErrorWithStatus:@"支付失败"];
    }
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

#pragma mark - UITableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return products.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SimpleCellIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleCellIdentifier"];
    }
    
    SKProduct *product = [products objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@%@)", product.localizedTitle, product.priceLocale.currencySymbol, [ViewUtils formatAmount:product.price.doubleValue], nil];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SKProduct *product = [products objectAtIndex:indexPath.row];
    [[SKPaymentQueue defaultQueue] addPayment:[SKPayment paymentWithProduct:product]];
}

@end
