//
//  HistoryPrepareViewController.m
//  Currency
//
//  Created by Tianyu An on 2017/4/9.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import "HistoryPrepareViewController.h"
#import "PickTableViewController.h"
#import "HistoryTableViewController.h"
#import "Currency.h"
#import "DBManager.h"
#import "ViewUtils.h"

@interface HistoryPrepareViewController () {
    Currency *currency;
    NSString *startDate;
    NSString *endDate;
    BOOL isStartDate;
}

@end

@implementation HistoryPrepareViewController

@synthesize flagImageView;
@synthesize currencyLabel;
@synthesize startLabel;
@synthesize endLabel;
@synthesize pickerLayout;
@synthesize datePicker;

- (void)initData {
    currency = [[DBManager getInstance] getCurrencyByCode:@"USD"];
    startDate = [ViewUtils getCurrentTime];
    endDate = startDate;
}

- (void)initView {
    self.title = @"历史汇率";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self refreshCurrencyView];
    startLabel.text = startDate;
    endLabel.text = endDate;
    
    datePicker.backgroundColor = [UIColor whiteColor];
    if (@available(iOS 13.4, *)) {
        // make date picker align to left in Xcode 13 with iOS 15
        datePicker.datePickerMode = UIDatePickerModeDate;
        datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
    }
}

- (IBAction)currencyViewClicked:(id)sender {
    PickTableViewController *controller = [[PickTableViewController alloc] init];
    controller.chosenCurrency = currency;
    controller.delegate = self;
    [ViewUtils pushViewController:controller target:self];
}

- (IBAction)startViewClicked:(id)sender {
    isStartDate = YES;
    [self showDatePickerWithDate:startDate];
}

- (IBAction)endViewClicked:(id)sender {
    isStartDate = NO;
    [self showDatePickerWithDate:endDate];
}

- (IBAction)historyButtonClicked:(id)sender {
    HistoryTableViewController *controller = [[HistoryTableViewController alloc] init];
    controller.chosenCurrency = currency;
    controller.startDate = startDate;
    controller.endDate = endDate;
    [ViewUtils pushViewController:controller target:self];
}

- (IBAction)confirmButtonClicked:(id)sender {
    [self dismissDatePicker:sender];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    if (isStartDate) {
        startDate = [formatter stringFromDate:datePicker.date];
        startLabel.text = startDate;
    } else {
        endDate = [formatter stringFromDate:datePicker.date];
        endLabel.text = endDate;
    }
}

- (IBAction)dismissDatePicker:(id)sender {
    pickerLayout.hidden = YES;
}

- (void)showDatePickerWithDate:(NSString *)dateString {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:[NSDate date]];
    components.year = [[dateString substringToIndex:4] intValue];
    components.month = [[dateString substringWithRange:NSMakeRange(5, 2)] intValue];
    components.day = [[dateString substringFromIndex:8] intValue];
    
    [datePicker setDate:[calendar dateFromComponents:components] animated:NO];
    pickerLayout.hidden = NO;
}

- (void)refreshCurrencyView {
    flagImageView.image = [UIImage imageNamed:currency.flag];
    currencyLabel.text = [ViewUtils getInfoString:currency];
}

#pragma mark - Delegate
- (void)passCurrency:(Currency *)chosenCurrency {
    currency = chosenCurrency;
    [self refreshCurrencyView];
}

@end
