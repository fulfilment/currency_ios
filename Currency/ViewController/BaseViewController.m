//
//  BaseViewController.m
//  Currency
//
//  Created by Tianyu An on 2017/3/12.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initView];
}

- (void)initData {}

- (void)initView {}

@end
