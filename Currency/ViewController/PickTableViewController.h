//
//  PickTableViewController.h
//  Currency
//
//  Created by Tianyu An on 2017/4/23.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import "BaseTableViewController.h"
#import "Currency.h"
#import "PassCurrencyDelegate.h"

@interface PickTableViewController : BaseTableViewController

@property (nonatomic, strong) Currency *chosenCurrency;
@property (nonatomic) NSObject<PassCurrencyDelegate> *delegate;

@end
