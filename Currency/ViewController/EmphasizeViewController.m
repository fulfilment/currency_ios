//
//  EmphasizeViewController.m
//  Currency
//
//  Created by Tianyu An on 2016/12/13.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import "EmphasizeViewController.h"
#import "ViewUtils.h"
#import "Currency.h"
#import "CurrencyDefaults.h"

@interface EmphasizeViewController () {
    CurrencyDefaults *defaults;
    UIImageView *middleImageView;
    UIImageView *cashSellingImageView;
    UIImageView *cashBuyingImageView;
    UIImageView *sellingImageView;
    UIImageView *buyingImageView;
    int type;
}
@end

@implementation EmphasizeViewController

- (void)initData {
    defaults = [CurrencyDefaults getInstance];
    type = [defaults getEmphasizeType];
}

- (void)initView {
    self.title = @"突出显示价格";
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F8);
    
    UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonClicked)];
    self.navigationItem.rightBarButtonItem = saveButtonItem;
    
    double startY = 18;
    
    [self.view addSubview:[ViewUtils getDividerInPositionY:startY]];
    startY += DIVIDER_HEIGHT;
    
    middleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(PHONE_WIDTH - 30, (LAYOUT_HEIGHT - 20) / 2, 20, 20)];
    middleImageView.hidden = type == TYPE_MIDDLE ? NO : YES;
    [middleImageView setImage:[UIImage imageNamed:@"Check"]];
    
    UIView *middleLayout = [ViewUtils getLayout:self title:@"中间价" positionY:startY];
    middleLayout.tag = TYPE_MIDDLE;
    [middleLayout addSubview:middleImageView];
    
    UITapGestureRecognizer *middleTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(choose:)];
    [middleLayout addGestureRecognizer:middleTapGesture];
    
    [self.view addSubview:middleLayout];
    startY += LAYOUT_HEIGHT;
    
    [self.view addSubview:[ViewUtils getDividerInPositionY:startY]];
    startY += DIVIDER_HEIGHT;
    
    cashSellingImageView = [[UIImageView alloc] initWithFrame:CGRectMake(PHONE_WIDTH - 30, (LAYOUT_HEIGHT - 20) / 2, 20, 20)];
    cashSellingImageView.hidden = type == TYPE_CASH_SELLING ? NO : YES;
    [cashSellingImageView setImage:[UIImage imageNamed:@"Check"]];
    
    UIView *cashSellingLayout = [ViewUtils getLayout:self title:@"现钞卖出价" positionY:startY];
    cashSellingLayout.tag = TYPE_CASH_SELLING;
    [cashSellingLayout addSubview:cashSellingImageView];
    
    UITapGestureRecognizer *cashSellingTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(choose:)];
    [cashSellingLayout addGestureRecognizer:cashSellingTapGesture];
    
    [self.view addSubview:cashSellingLayout];
    startY += LAYOUT_HEIGHT;
    
    [self.view addSubview:[ViewUtils getDividerInPositionY:startY]];
    startY += DIVIDER_HEIGHT;
    
    cashBuyingImageView = [[UIImageView alloc] initWithFrame:CGRectMake(PHONE_WIDTH - 30, (LAYOUT_HEIGHT - 20) / 2, 20, 20)];
    cashBuyingImageView.hidden = type == TYPE_CASH_BUYING ? NO : YES;
    [cashBuyingImageView setImage:[UIImage imageNamed:@"Check"]];
    
    UIView *cashBuyingLayout = [ViewUtils getLayout:self title:@"现钞买入价" positionY:startY];
    cashBuyingLayout.tag = TYPE_CASH_BUYING;
    [cashBuyingLayout addSubview:cashBuyingImageView];
    
    UITapGestureRecognizer *cashBuyingTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(choose:)];
    [cashBuyingLayout addGestureRecognizer:cashBuyingTapGesture];
    
    [self.view addSubview:cashBuyingLayout];
    startY += LAYOUT_HEIGHT;
    
    [self.view addSubview:[ViewUtils getDividerInPositionY:startY]];
    startY += DIVIDER_HEIGHT;
    
    sellingImageView = [[UIImageView alloc] initWithFrame:CGRectMake(PHONE_WIDTH - 30, (LAYOUT_HEIGHT - 20) / 2, 20, 20)];
    sellingImageView.hidden = type == TYPE_SELLING ? NO : YES;
    [sellingImageView setImage:[UIImage imageNamed:@"Check"]];
    
    UIView *sellingLayout = [ViewUtils getLayout:self title:@"现汇卖出价" positionY:startY];
    sellingLayout.tag = TYPE_SELLING;
    [sellingLayout addSubview:sellingImageView];
    
    UITapGestureRecognizer *sellingTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(choose:)];
    [sellingLayout addGestureRecognizer:sellingTapGesture];
    
    [self.view addSubview:sellingLayout];
    startY += LAYOUT_HEIGHT;
    
    [self.view addSubview:[ViewUtils getDividerInPositionY:startY]];
    startY += DIVIDER_HEIGHT;
    
    buyingImageView = [[UIImageView alloc] initWithFrame:CGRectMake(PHONE_WIDTH - 30, (LAYOUT_HEIGHT - 20) / 2, 20, 20)];
    buyingImageView.hidden = type == TYPE_BUYING ? NO : YES;
    [buyingImageView setImage:[UIImage imageNamed:@"Check"]];
    
    UIView *buyingLayout = [ViewUtils getLayout:self title:@"现汇买入价" positionY:startY];
    buyingLayout.tag = TYPE_BUYING;
    [buyingLayout addSubview:buyingImageView];
    
    UITapGestureRecognizer *buyingTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(choose:)];
    [buyingLayout addGestureRecognizer:buyingTapGesture];
    
    [self.view addSubview:buyingLayout];
    startY += LAYOUT_HEIGHT;
    
    [self.view addSubview:[ViewUtils getDividerInPositionY:startY]];
    startY += DIVIDER_HEIGHT;
}

- (void)saveButtonClicked {
    [defaults setEmphasizeType:type];
    [defaults synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)hideAllImageViews {
    middleImageView.hidden = YES;
    cashSellingImageView.hidden = YES;
    cashBuyingImageView.hidden = YES;
    sellingImageView.hidden = YES;
    buyingImageView.hidden = YES;
}

- (void)choose:(UITapGestureRecognizer *)gesture {
    [self hideAllImageViews];
    type = (int) gesture.view.tag;
    switch (type) {
        case TYPE_MIDDLE:
            middleImageView.hidden = NO;
            break;
        case TYPE_CASH_SELLING:
            cashSellingImageView.hidden = NO;
            break;
        case TYPE_CASH_BUYING:
            cashBuyingImageView.hidden = NO;
            break;
        case TYPE_SELLING:
            sellingImageView.hidden = NO;
            break;
        case TYPE_BUYING:
            buyingImageView.hidden = NO;
            break;
    }
}

@end
