//
//  SettingsViewController.m
//  Currency
//
//  Created by lichangyuan on 16/2/25.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import "SettingsViewController.h"
#import "EmphasizeViewController.h"
#import "ChooseViewController.h"
#import "WidgetTableViewController.h"
#import "MainTableViewController.h"
#import "ViewUtils.h"
#import "Currency.h"
#import "CurrencyDefaults.h"

@interface SettingsViewController () {
    CurrencyDefaults *defaults;
    UILabel *emphasizeLabel;
    UILabel *widgetLabel;
}
@end

static const int TAG_CHOOSE_UNIT = 0;
static const int TAG_EMPHASIZE = 1;
static const int TAG_CHOOSE_WIDGET = 2;

@implementation SettingsViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    int type = [defaults getEmphasizeType];
    emphasizeLabel.text = [Currency getTypeString:type isShort:NO];
    widgetLabel.text = [defaults getEmphasizeName];
}

- (void)initData {
    defaults = [CurrencyDefaults getInstance];
}

- (void)initView {
    self.title = @"设置";
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F8);

    double startY = 18;
    
    [self.view addSubview:[ViewUtils getDividerInPositionY:startY]];
    startY += DIVIDER_HEIGHT;
    
    UIImageView *nextImageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(PHONE_WIDTH - 18, (LAYOUT_HEIGHT - 15) / 2, 8, 15)];
    [nextImageView1 setImage:[UIImage imageNamed:@"Next"]];
    
    UIView *chooseLayout = [ViewUtils getLayout:self title:@"选择与排列货币" positionY:startY];
    chooseLayout.tag = TAG_CHOOSE_UNIT;
    [chooseLayout addSubview:nextImageView1];
    UITapGestureRecognizer *chooseTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [chooseLayout addGestureRecognizer:chooseTapGesture];
    
    [self.view addSubview:chooseLayout];
    startY += LAYOUT_HEIGHT;
    
    [self.view addSubview:[ViewUtils getDividerInPositionY:startY]];
    startY += DIVIDER_HEIGHT;
    
    emphasizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(PHONE_WIDTH - 125, (LAYOUT_HEIGHT - 24) / 2, 100, 24)];
    emphasizeLabel.textColor = UIColorFromRGB(0x666666);
    emphasizeLabel.font = [UIFont systemFontOfSize:16];
    emphasizeLabel.textAlignment = NSTextAlignmentRight;
    
    UIImageView *nextImageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(PHONE_WIDTH - 18, (LAYOUT_HEIGHT - 15) / 2, 8, 15)];
    [nextImageView2 setImage:[UIImage imageNamed:@"Next"]];
    
    UIView *emphasizeLayout = [ViewUtils getLayout:self title:@"突出显示价格" positionY:startY];
    emphasizeLayout.tag = TAG_EMPHASIZE;
    [emphasizeLayout addSubview:emphasizeLabel];
    [emphasizeLayout addSubview:nextImageView2];
    UITapGestureRecognizer *emphasizeTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [emphasizeLayout addGestureRecognizer:emphasizeTapGesture];
    
    [self.view addSubview:emphasizeLayout];
    startY += LAYOUT_HEIGHT;
    
    [self.view addSubview:[ViewUtils getDividerInPositionY:startY]];
    startY += DIVIDER_HEIGHT;
    
    widgetLabel = [[UILabel alloc] initWithFrame:CGRectMake(PHONE_WIDTH - 145, (LAYOUT_HEIGHT - 24) / 2, 120, 24)];
    widgetLabel.textColor = UIColorFromRGB(0x666666);
    widgetLabel.font = [UIFont systemFontOfSize:16];
    widgetLabel.textAlignment = NSTextAlignmentRight;
    
    UIImageView *nextImageView3 = [[UIImageView alloc] initWithFrame:CGRectMake(PHONE_WIDTH - 18, (LAYOUT_HEIGHT - 15) / 2, 8, 15)];
    [nextImageView3 setImage:[UIImage imageNamed:@"Next"]];
    
    UIView *widgetLayout = [ViewUtils getLayout:self title:@"选择Widget展示货币" positionY:startY];
    widgetLayout.tag = TAG_CHOOSE_WIDGET;
    [widgetLayout addSubview:widgetLabel];
    [widgetLayout addSubview:nextImageView3];
    UITapGestureRecognizer *widgetTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [widgetLayout addGestureRecognizer:widgetTapGesture];
    
    [self.view addSubview:widgetLayout];
    startY += LAYOUT_HEIGHT;
    
    [self.view addSubview:[ViewUtils getDividerInPositionY:startY]];
}

- (void)tap:(UITapGestureRecognizer *)gesture {
    UIViewController *controller;
    if (gesture.view.tag == TAG_CHOOSE_UNIT) {
        controller = [[ChooseViewController alloc] init];
    } else if (gesture.view.tag == TAG_EMPHASIZE) {
        controller = [[EmphasizeViewController alloc] init];
    } else {
        controller = [[WidgetTableViewController alloc] init];
    }
    
    [ViewUtils pushViewController:controller target:self];
}

@end
