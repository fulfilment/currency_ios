//
//  MainTableViewController.h
//  Currency
//
//  Created by lichangyuan on 16/1/29.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

@interface MainTableViewController : BaseTableViewController<NSURLConnectionDataDelegate>

@end
