//
//  Person.h
//  AccountCleaner
//
//  Created by Tianyu An on 14-8-2.
//  Copyright (c) 2014年 Tianyu An. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

static const int TYPE_MIDDLE = 0;
static const int TYPE_CASH_SELLING = 1;
static const int TYPE_CASH_BUYING = 2;
static const int TYPE_SELLING = 3;
static const int TYPE_BUYING = 4;

@interface Currency : NSObject

/**
 * 货币的中文名字，由创建时确定，非空且不可改变，如美元、英镑
 * 如果一定要更改货币名字，应该删除原有货币，再添加新货币
 */
@property (nonatomic, strong) NSString *name;

/**
 * 货币的代码，如美元:USD、英镑:GBP
 */
@property(nonatomic, strong) NSString *code;

/**
 * 货币的符号，如美元:$、英镑:£，存储时注意编码格式
 */
@property(nonatomic, strong) NSString *symbol;

/**
 * 货币所属国家的国旗，存储图片名字
 */
@property (nonatomic, strong) NSString *flag;

/**
 * 中间价
 */
@property(nonatomic, assign) CGFloat middleRate;

/**
 * 钞卖
 */
@property(nonatomic, assign) CGFloat cashSellingRate;

/**
 * 钞买
 */
@property(nonatomic, assign) CGFloat cashBuyingRate;

/**
 * 汇卖
 */
@property(nonatomic, assign) CGFloat sellingRate;

/**
 * 汇买
 */
@property(nonatomic, assign) CGFloat buyingRate;

/**
 * 是否显示在列表中
 */
@property(nonatomic, assign) BOOL isShown;

/**
 * 在列表中的显示位置
 * 显示的currency隐藏后继续保持show index，再次加入到显示列表后显示在原来位置
 */
@property(nonatomic, assign) int showIndex;

/**
 * 更新时间，历史汇率中使用
 */
@property (nonatomic, strong) NSString *updateTime;

#pragma mark - init
- (id)init;
- (id)initWithName:(NSString *)currencyName code:(NSString *)currencyCode symbol:(NSString *)currencySymbol flag:(NSString *)currencyFlag showIndex:(int)index;
- (id)initWithName:(NSString *)currencyName middle:(CGFloat)middle buying:(CGFloat)buying selling:(CGFloat)selling cashBuying:(CGFloat)cashBuying cashSelling:(CGFloat)cashSelling;
- (id)initWithUpdateTime:(NSString *)time middle:(CGFloat)middle buying:(CGFloat)buying selling:(CGFloat)selling cashBuying:(CGFloat)cashBuying cashSelling:(CGFloat)cashSelling;

#pragma mark - static methods
+ (NSString *)getTypeString:(int)type isShort:(BOOL)isShort;
+ (Currency *)getCurrencyFromList:(NSArray *)currencyList byCode:(NSString *)code;

@end
