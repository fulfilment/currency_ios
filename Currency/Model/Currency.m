//
//  Person.m
//  AccountCleaner
//
//  Created by Tianyu An on 14-8-2.
//  Copyright (c) 2014年 Tianyu An. All rights reserved.
//

#import "Currency.h"

@implementation Currency

@synthesize name;
@synthesize code;
@synthesize symbol;
@synthesize flag;
@synthesize middleRate;
@synthesize buyingRate;
@synthesize sellingRate;
@synthesize cashBuyingRate;
@synthesize cashSellingRate;
@synthesize isShown;
@synthesize showIndex;
@synthesize updateTime;

- (id)init {
    if (self = [super init]) {
        self.name = @"";
        self.code = @"";
        self.symbol = @"";
        self.flag = @"";
        self.middleRate = 0;
        self.buyingRate = 0;
        self.sellingRate = 0;
        self.cashBuyingRate = 0;
        self.cashSellingRate = 0;
        self.isShown = YES;
        self.showIndex = -1;
        self.updateTime = @"";
    }
    
    return self;
}

- (id)initWithName:(NSString *)currencyName code:(NSString *)currencyCode symbol:(NSString *)currencySymbol flag:(NSString *)currencyFlag showIndex:(int)index {
    if (self = [super init]) {
        self = [self init];
        self.name = currencyName;
        self.code = currencyCode;
        self.symbol = currencySymbol;
        self.flag = currencyFlag;
        self.showIndex = index;
    }
    
    return self;
}

- (id)initWithName:(NSString *)currencyName middle:(CGFloat)middle buying:(CGFloat)buying selling:(CGFloat)selling cashBuying:(CGFloat)cashBuying cashSelling:(CGFloat)cashSelling {
    if (self = [super init]) {
        self = [self init];
        self.name = currencyName;
        self.middleRate = middle;
        self.buyingRate = buying;
        self.sellingRate = selling;
        self.cashBuyingRate = cashBuying;
        self.cashSellingRate = cashSelling;
    }
    
    return self;
}

- (id)initWithUpdateTime:(NSString *)time middle:(CGFloat)middle buying:(CGFloat)buying selling:(CGFloat)selling cashBuying:(CGFloat)cashBuying cashSelling:(CGFloat)cashSelling {
    if (self = [super init]) {
        self = [self init];
        self.middleRate = middle;
        self.buyingRate = buying;
        self.sellingRate = selling;
        self.cashBuyingRate = cashBuying;
        self.cashSellingRate = cashSelling;
        self.updateTime = time;
    }
    
    return self;
}

+ (NSString *)getTypeString:(int)type isShort:(BOOL)isShort {
    switch (type) {
        case TYPE_CASH_SELLING:
            return isShort ? @"钞卖" : @"现钞卖出价";
        case TYPE_CASH_BUYING:
            return isShort ? @"钞买" : @"现钞买入价";
        case TYPE_SELLING:
            return isShort ? @"汇卖" : @"现汇卖出价";
        case TYPE_BUYING:
            return isShort ? @"汇买" : @"现汇买入价";
        default:
            return @"中间价";
    }
}

+ (Currency *)getCurrencyFromList:(NSArray *)currencyList byCode:(NSString *)code {
    for (int i = 0; i < currencyList.count; i++) {
        Currency *currency = [currencyList objectAtIndex:i];
        if ([currency.code caseInsensitiveCompare:code] == NSOrderedSame) {
            return currency;
        }
    }
    return nil;
}

@end
