//
//  ChatLabel.h
//  AccountCleaner
//
//  Created by Tianyu An on 2018/4/21.
//  Copyright © 2018年 Tianyu An. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ChatLabelType) {
    ChatLabelTypeMine = 0,
    ChatLabelTypeOthers
};

@interface ChatLabel : UIView

@property (strong, nonatomic) UILabel *contentLabel;

- (instancetype)initWithFrame:(CGRect)frame type:(ChatLabelType)type;

- (void)addHorizontalContraints;

@end
