//
//  MainTableViewCell.m
//  Currency
//
//  Created by lichangyuan on 16/1/29.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import "MainTableViewCell.h"
#import "ViewUtils.h"

@implementation MainTableViewCell

@synthesize flagImageView;
@synthesize nameLabel;
@synthesize symbolLabel;
@synthesize middleLabel;
@synthesize cashSellingLabel;
@synthesize cashBuyingLabel;
@synthesize sellingLabel;
@synthesize buyingLabel;

- (void)setCurrency:(Currency *)currency {
    flagImageView.image = [UIImage imageNamed:currency.flag];
    nameLabel.text = currency.name;
    symbolLabel.text = [NSString stringWithFormat:@"%@ (%@)", currency.code, currency.symbol];
    
    [ViewUtils setRateLabel:middleLabel rate:currency.middleRate];
    [ViewUtils setRateLabel:cashSellingLabel rate:currency.cashSellingRate];
    [ViewUtils setRateLabel:cashBuyingLabel rate:currency.cashBuyingRate];
    [ViewUtils setRateLabel:sellingLabel rate:currency.sellingRate];
    [ViewUtils setRateLabel:buyingLabel rate:currency.buyingRate];
}

@end
