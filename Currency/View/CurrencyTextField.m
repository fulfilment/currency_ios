//
//  CurrencyTextField.m
//  Currency
//
//  Created by Tianyu An on 16/3/26.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import "CurrencyTextField.h"
#import "ViewUtils.h"

@implementation CurrencyTextField

- (void)drawRect:(CGRect)rect {
    self.borderStyle = UITextBorderStyleNone;
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, UIColorFromRGB(0x9C9C9C).CGColor);
    CGContextFillRect(context, CGRectMake(0, CGRectGetHeight(self.frame) - 0.5, CGRectGetWidth(self.frame), 0.5));
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    if (action == @selector(paste:)) {
        return NO;
    }
    return [super canPerformAction:action withSender:sender];
}

@end
