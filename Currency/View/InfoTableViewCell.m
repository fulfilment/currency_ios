//
//  TitleTableViewCell.m
//  Currency
//
//  Created by Tianyu An on 16/3/29.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import "InfoTableViewCell.h"

@implementation InfoTableViewCell

@synthesize timeLabel;

- (void)setTime:(NSString *)time {
    timeLabel.text = time;
}

@end
