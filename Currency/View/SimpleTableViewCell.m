//
//  WidgetTableViewCell.m
//  Currency
//
//  Created by Tianyu An on 2017/1/7.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import "SimpleTableViewCell.h"
#import "ViewUtils.h"

@implementation SimpleTableViewCell

@synthesize flagImageView;
@synthesize currencyLabel;

- (void)setCurrency:(Currency *)currency {
    flagImageView.image = [UIImage imageNamed:currency.flag];
    currencyLabel.text = [ViewUtils getInfoString:currency];
}

@end
