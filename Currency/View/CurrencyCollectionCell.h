//
//  CurrencyCollectionViewCell.h
//  Currency
//
//  Created by Tianyu An on 16/3/29.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Currency.h"

@interface CurrencyCollectionCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *symbolLabel;
@property (strong, nonatomic) IBOutlet UIImageView *flagImageView;
@property (strong, nonatomic) IBOutlet UIView *coverView;

- (void)setCurrency:(Currency *)currency;

@end