//
//  CurrencyCollectionViewCell.m
//  Currency
//
//  Created by Tianyu An on 16/3/29.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import "CurrencyCollectionCell.h"

@implementation CurrencyCollectionCell

@synthesize nameLabel;
@synthesize symbolLabel;
@synthesize flagImageView;
@synthesize coverView;

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)setCurrency:(Currency *)currency {
    nameLabel.text = currency.name;
    symbolLabel.text = [NSString stringWithFormat:@"%@ (%@)", currency.code, currency.symbol];
    flagImageView.image = [UIImage imageNamed:currency.flag];
    coverView.hidden = !currency.isShown;
}

@end
