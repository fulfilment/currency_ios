//
//  TitleTableViewCell.h
//  Currency
//
//  Created by Tianyu An on 16/3/29.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

- (void)setTime:(NSString *)time;

@end
