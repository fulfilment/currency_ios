//
//  HistoryTableViewCell.h
//  Currency
//
//  Created by Tianyu An on 2017/4/23.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Currency.h"

@interface HistoryTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *updateTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *middleLabel;
@property (strong, nonatomic) IBOutlet UILabel *cashSellingLabel;
@property (strong, nonatomic) IBOutlet UILabel *cashBuyingLabel;
@property (strong, nonatomic) IBOutlet UILabel *sellingLabel;
@property (strong, nonatomic) IBOutlet UILabel *buyingLabel;

- (void)setCurrency:(Currency *)currency;

@end
