//
//  WidgetTableViewCell.h
//  Currency
//
//  Created by Tianyu An on 2017/1/7.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Currency.h"

@interface SimpleTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *flagImageView;
@property (strong, nonatomic) IBOutlet UILabel *currencyLabel;

- (void)setCurrency:(Currency *)currency;

@end
