//
//  ChatLabel.m
//  AccountCleaner
//
//  Created by Tianyu An on 2018/4/21.
//  Copyright © 2018年 Tianyu An. All rights reserved.
//

#import "ChatLabel.h"
#import "ViewUtils.h"
#import "Masonry.h"

const int CORNER_RADIUS = 15;
const int MARGIN_SMALL = 15;
const int MARGIN_LARGE = 40;

@interface ChatLabel ()

@property (nonatomic, assign) NSInteger type;

@end

@implementation ChatLabel

- (instancetype)initWithFrame:(CGRect)frame type:(ChatLabelType)type {
    self = [super initWithFrame:frame];
    if (self) {
        _type = type;
        
        self.backgroundColor = _type == ChatLabelTypeMine ? MARKED_COLOR : MAJOR_COLOR;
        self.layer.masksToBounds = YES;
        
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _contentLabel.numberOfLines = 0;
        _contentLabel.textColor = [UIColor whiteColor];
        _contentLabel.font = [UIFont systemFontOfSize:16];
        [self addSubview:_contentLabel];
        [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(CORNER_RADIUS);
            make.right.equalTo(self.mas_right).offset(-CORNER_RADIUS);
            make.top.equalTo(self.mas_top).offset(CORNER_RADIUS);
            make.bottom.equalTo(self.mas_bottom).offset(-CORNER_RADIUS);
        }];
    }
    return self;
}

- (void)layoutSublayersOfLayer:(CALayer *)layer {
    [super layoutSublayersOfLayer:layer];
    if (@available(iOS 11.0, *)) {
        if (_type == ChatLabelTypeMine) {
            layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMaxXMinYCorner | kCALayerMinXMinYCorner;
        } else {
            layer.maskedCorners = kCALayerMaxXMaxYCorner | kCALayerMaxXMinYCorner | kCALayerMinXMinYCorner;
        }
        layer.cornerRadius = CORNER_RADIUS;
    } else {
        UIRectCorner corners = _type == ChatLabelTypeMine ? UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft : UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomRight;
        CAShapeLayer *cornersLayer = [CAShapeLayer layer];
        cornersLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                  byRoundingCorners:corners
                                                        cornerRadii:(CGSize){CORNER_RADIUS, CORNER_RADIUS}].CGPath;
        [layer setMask:cornersLayer];
    }
}

- (void)addHorizontalContraints {
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        if (self.type == ChatLabelTypeMine) {
            make.left.greaterThanOrEqualTo(self.superview.mas_left).offset(MARGIN_LARGE);
            make.right.equalTo(self.superview.mas_right).offset(-MARGIN_SMALL);
        } else {
            make.left.equalTo(self.superview.mas_left).offset(MARGIN_SMALL);
            make.right.lessThanOrEqualTo(self.superview.mas_right).offset(-MARGIN_LARGE);
        }
    }];
}

@end
