//
//  MainTableViewCell.h
//  Currency
//
//  Created by lichangyuan on 16/1/29.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Currency.h"

@interface MainTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *symbolLabel;
@property (weak, nonatomic) IBOutlet UILabel *middleLabel;
@property (weak, nonatomic) IBOutlet UILabel *cashSellingLabel;
@property (weak, nonatomic) IBOutlet UILabel *cashBuyingLabel;
@property (weak, nonatomic) IBOutlet UILabel *sellingLabel;
@property (weak, nonatomic) IBOutlet UILabel *buyingLabel;

- (void)setCurrency:(Currency *)currency;

@end
