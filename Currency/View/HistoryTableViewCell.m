//
//  HistoryTableViewCell.m
//  Currency
//
//  Created by Tianyu An on 2017/4/23.
//  Copyright © 2017年 Tianyu An. All rights reserved.
//

#import "HistoryTableViewCell.h"
#import "ViewUtils.h"

@implementation HistoryTableViewCell

@synthesize updateTimeLabel;
@synthesize middleLabel;
@synthesize cashSellingLabel;
@synthesize cashBuyingLabel;
@synthesize sellingLabel;
@synthesize buyingLabel;

- (void)setCurrency:(Currency *)currency {
    updateTimeLabel.text = currency.updateTime;
    [ViewUtils setRateLabel:middleLabel rate:currency.middleRate];
    [ViewUtils setRateLabel:cashSellingLabel rate:currency.cashSellingRate];
    [ViewUtils setRateLabel:cashBuyingLabel rate:currency.cashBuyingRate];
    [ViewUtils setRateLabel:sellingLabel rate:currency.sellingRate];
    [ViewUtils setRateLabel:buyingLabel rate:currency.buyingRate];
}

@end
