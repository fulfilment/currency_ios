//
//  ExtensionViewController.m
//  extension
//
//  Created by Tianyu An on 2016/12/17.
//  Copyright © 2016年 Tianyu An. All rights reserved.
//

#import "ExtensionViewController.h"
#import <NotificationCenter/NotificationCenter.h>
#import "Currency.h"
#import "CurrencyRequest.h"
#import "CurrencyDefaults.h"
#import "DBManager.h"
#import "ViewUtils.h"

@interface ExtensionViewController () <NCWidgetProviding> {
    DBManager *dbManager;
    CurrencyDefaults *defaults;
    Currency *currency;
}

@end

@implementation ExtensionViewController

@synthesize flagImageView;
@synthesize currencyLabel;
@synthesize typeLabel;
@synthesize rateLabel;
@synthesize timeLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    [self loadData];
    [self updateData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.extensionContext.widgetLargestAvailableDisplayMode = NCWidgetDisplayModeCompact;
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData

    completionHandler(NCUpdateResultNewData);
}

- (UIEdgeInsets)widgetMarginInsetsForProposedMarginInsets:(UIEdgeInsets)defaultMarginInsets {
    return UIEdgeInsetsZero;
}

- (void)initView {
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(openApp:)]];
    
    dbManager = [DBManager getInstance];
    defaults = [CurrencyDefaults getInstance];
}

- (void)loadData {
    currency = [dbManager getCurrencyByCode:[defaults getEmphasizeCode]];
    
    [flagImageView setImage:[UIImage imageNamed:currency.flag]];
    
    currencyLabel.text = [NSString stringWithFormat:@"%@ %@(%@)", currency.name, currency.code, currency.symbol];
    
    int type = [defaults getEmphasizeType];
    typeLabel.text = [Currency getTypeString:type isShort:YES];
    
    CGFloat rate = 0;
    switch (type) {
        case TYPE_CASH_SELLING:
            rate = currency.sellingRate;
            break;
        case TYPE_CASH_BUYING:
            rate = currency.cashBuyingRate;
            break;
        case TYPE_SELLING:
            rate = currency.sellingRate;
            break;
        case TYPE_BUYING:
            rate = currency.buyingRate;
            break;
        default:
            rate = currency.middleRate;
            break;
    }
    
    NSString *rateString = rate == 0 ? @"    -" : [NSString stringWithFormat:@"¥%.2f", rate];
    rateLabel.text = rateString;
    
    timeLabel.text = [ViewUtils timestampToString:[defaults getLastUpdateTime]];
}

- (void)updateData {
    [CurrencyRequest getRate:^(NSMutableArray *results) {
        for (Currency *result in results) {
            [dbManager updateCurrency:result];
        }
        
        [defaults setLastUpdateTime:[[NSDate date] timeIntervalSince1970]];
        [defaults synchronize];
        [self loadData];
    } failure:nil];
}

- (void)openApp:(UITapGestureRecognizer *)gesture {
    NSString *url = [NSString stringWithFormat:@"currency://currency=%@", currency.code];
    [self.extensionContext openURL:[NSURL URLWithString:url] completionHandler:nil];
}

@end
